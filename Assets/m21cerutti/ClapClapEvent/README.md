# ClapClapEvent

A persistent custom event system for Unity.

Free & open source, please consider supporting ClapClapEvent and &#10024;<a href="https://ko-fi.com/m21cerutti">buy a cofee</a> &#10024;.

<a href="https://ko-fi.com/m21cerutti">Ko-Fi</a> |  <a href="https://twitter.com/m21cerutti">Twitter</a> | <a href="https://discord.com/invite/3qcacrhtxf">Discord</a>

Permit to handle events in your projects that are :
- Static (made offline)
- Persistent
- Cross-Scene
- With parameters
- Changing (easy to iterate)

For example, UI events, Game Logic events, Scene events, ...

The main selling point is that in order to test you don't need that your events are called from code, or that a listener is linked to your events.
Really adapted if you use an additive scenes workflow.

<img src="./Data/MainInterface.png" alt="MainInterface"/>

Features: 
- Create event
- Call event from editor (generally for simple types)
- Modify event
- Register function by name (shortcut)
- Editor that track those used in scenes
- Debugs (registers, calls, ...)
- Samples scenes

## Installation
---

Import the UnityPackage in your project and it's done, you will just have a copy of the ScriptTemplates folders used inside projects (since Unity accept them only if there are in the root ScriptTemplates folder).


<div style="page-break-after: always;"></div>

## Quick use
---

Open the editor in Tools > ClapClapEvent > Manager and refresh the project events.

<img src="./Data/ClapClapEventManager_1.png" alt="Open Manager" width="400"/>

Here you see the events created in "Project Events", with callers and listeners associated if they have referenced in scene. You can also filter the results with "Excluded Events" that hide the ones you don't want.
You can create events, and add listeners to GameObjects selected in scene.

<img src="./Data/ClapClapEventManager_2.png" alt="Manager" width="400"/>

For instance, in order to create a event that activate/deactivate object that are listenning, select a "BoolEvent" for the type and click on CreateEvent, choose the name of the Event, like "DeactivateEvent" (watchout the name suffix must be "Event" in order to have some utilities ) and Click after that on the GameObject you want to listen. Click on "Add listener to selected" in order to add the event (that have been set on the Event field).
Finnally, go on the Inspector and on the BoolEventReceiver, add a callback on the UnityEvent to GameObject>SetActive. You can see the difference between dynamic and static parameter.


<div style="page-break-after: always;"></div>

## Events, Caller and Receiver
---

You can create Events from two ways, from the context menu in hierarchy Create>Events or from the Editor.
The created Events are strictly typed and only can be associated with the receiver of the same type.

In order to call an event, you can first test it from the inspector of the event (recommended when playing) or from the code by referencing it. For example see [SenderMessage](./Samples/Scripts/SenderMessage.cs) in the samples.

```csharp
public TypeEvent testEvent;
...
public void SendEvent()
{
    Type parameter;
    testEvent.Invoke(parameter);
}
```

You can add the event to receiver from the ClapClapEventManager, but also from the Inspector by drag and drop in the listeners list, or with the "AddEvent" button (from the field it don't find the created events due to Unity limitations).

<img src="./Data/Receiver.png" alt="Receiver"/>

Than you can register local functions automatically if they respect the convention name of event (or a warning will be trigger), for instance, ***ALongNameEvent*** will register ***OnALongName*** functions on the GameObject, the parameters of the function must also be the same as the Event (special case of SimpleEvent with no parameters).
Otherwise you can register manually the functions from UnityEvent controls.

<div style="page-break-after: always;"></div>

### Architecture

```plantuml
@startuml

usecase Caller #Red
usecase Event #Green
usecase UnityEvent #Cyan
usecase Callback #Cyan
usecase Callback_X #Cyan
usecase Receiver_X
usecase Receiver as "Receiver
--
Listeners[]"


Caller --> Event : "Invoke"
Event ..> Receiver
Event ..> Receiver_X

Receiver --> UnityEvent : "Dispatch locally"

UnityEvent --> Callback
UnityEvent --> Callback_X
@enduml
```

<div style="page-break-after: always;"></div>

## Advanced
---

<img src="./Data/Advanced.png" alt="Advanced"/>

### Debug

You can change the debug level from the ClapClapEventManager, by default it's set on Warning. You can also change the colors for debugs.

### Create custom type event

In order to create your own types, you can use in the editor the advanced section that permit also to create default inspectors with it if you want. Type the type of event you want and modify the files created in order to suit your needs.

### Change existing event type

In order to replace an already existing event to another type, you can select a event and the new type in the advanced section. You will then have a resume log in the console of all GameObjects and components that need to be fill again ( if it was strictly typed). You need also clean the old receiver of the old type.

## Support
---

marc.cerutti@outlook.fr

or 

Discord :
https://discord.gg/Ga7dWXJNRV
