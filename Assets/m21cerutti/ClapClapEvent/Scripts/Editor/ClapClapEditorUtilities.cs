﻿namespace m21cerutti.ClapClapEvent.Editor {
	using System;
	using System.Reflection;

	using Runtime;

	using UnityEditor;

	using UnityEngine;

	public static class ClapClapEditorUtilities {
		// ! Not safe code ! Don't change function names
		public static void CallAddEventToListenerUnsafe(EventClapClap ev, GameObject g) {
			Type event_type = ev.GetType();
			Type receiver_type = Type.GetType(event_type.FullName + "Receiver, " + event_type.Assembly, true);
			Component c = g.GetComponent(receiver_type);
			if (c == null) {
				c = Undo.AddComponent(g, receiver_type);
			}
			MethodInfo method = receiver_type.GetMethod("AddPersistentEventHandler");
			method.Invoke(c, new object[] {ev});
		}
	}
}