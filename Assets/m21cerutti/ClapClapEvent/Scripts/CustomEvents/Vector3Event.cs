namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine;

	[CreateAssetMenu(fileName = "Vector3Event", menuName = "Events/Vector3Event")]
	public class Vector3Event : ParametrisedEvent<Vector3> {}
}