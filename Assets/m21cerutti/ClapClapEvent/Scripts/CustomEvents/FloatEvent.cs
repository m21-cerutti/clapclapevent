namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine;

	public delegate void FloatEventCallback(float param);

	[CreateAssetMenu(fileName = "FloatEvent", menuName = "Events/FloatEvent")]
	public class FloatEvent : ParametrisedEvent<float> {}
}