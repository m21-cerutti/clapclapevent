﻿namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	public class BoolEventReceiver : MultiParamEventReceiver<bool> {}
}