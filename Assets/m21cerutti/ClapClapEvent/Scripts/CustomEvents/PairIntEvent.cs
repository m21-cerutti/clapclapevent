namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine;

	public struct PairInt {
		public int first;
		public int second;
	}

	[CreateAssetMenu(fileName = "PairIntEvent", menuName = "Events/PairIntEvent")]
	public class PairIntEvent : ParametrisedEvent<PairInt> {}
}