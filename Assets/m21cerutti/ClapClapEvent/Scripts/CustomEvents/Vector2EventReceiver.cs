namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine;

	public class Vector2EventReceiver : MultiParamEventReceiver<Vector2> {}
}