namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine;

	[CreateAssetMenu(fileName = "TransformEvent", menuName = "Events/TransformEvent")]
	public class TransformEvent : ParametrisedEvent<Transform> {}
}