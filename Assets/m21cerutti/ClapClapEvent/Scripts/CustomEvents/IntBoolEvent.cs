namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine;

	public struct IntBool {
		public int first;
		public bool second;
	}

	[CreateAssetMenu(fileName = "IntBoolEvent", menuName = "Events/IntBoolEvent")]
	public class IntBoolEvent : ParametrisedEvent<IntBool> {}
}