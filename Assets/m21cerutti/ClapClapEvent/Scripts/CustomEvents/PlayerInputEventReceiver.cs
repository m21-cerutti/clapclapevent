#if USE_NEWINPUT
namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine.InputSystem;

	public class PlayerInputEventReceiver : MultiParamEventReceiver<PlayerInput> { }
}
#endif