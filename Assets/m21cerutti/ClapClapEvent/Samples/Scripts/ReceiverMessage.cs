namespace m21cerutti.ClapClapEvent.Samples {
	using UnityEngine;
	using UnityEngine.UI;

	public class ReceiverMessage : MonoBehaviour {
		[SerializeField] private Text m_ui;

		public void OnMessage(string message) { m_ui.text = message; }
	}
}