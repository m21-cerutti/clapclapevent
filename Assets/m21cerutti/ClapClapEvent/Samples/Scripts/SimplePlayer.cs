namespace m21cerutti.ClapClapEvent.Samples {

	using CustomEvents;

	using UnityEngine;
	using UnityEngine.UI;
#if USE_NEWINPUT
	using UnityEngine.InputSystem;
	using static UnityEngine.InputSystem.DefaultInputActions;
#endif

	public class SimplePlayer : MonoBehaviour {
		[SerializeField] private GameObjectEvent m_playerEvent;
		[SerializeField] private float m_speed = 2.0f;
		[SerializeField] private float m_life;
		[SerializeField] private Text m_ui;

		private Rigidbody m_rb;
#if USE_NEWINPUT
		private DefaultInputActions m_PlayerActions;
		private Vector2 move;

		private void OnEnable() {
			m_PlayerActions.Enable();
		}
		
		void OnDisable() {
			m_PlayerActions.Disable();
		}

		private void Move(InputAction.CallbackContext ctx) {
			move = ctx.ReadValue<Vector2>();
		}
#endif

		private void Awake() {
#if USE_NEWINPUT
			m_PlayerActions = new DefaultInputActions();
			m_PlayerActions.Player.Move.performed += Move;
#endif
			m_life = 100;
			m_rb = GetComponent<Rigidbody>();
			m_playerEvent.Invoke(gameObject);
		}

		// Update is called once per frame
		private void Update() {
#if USE_NEWINPUT
			float x_move = move.x;
			float y_move = move.y;
#else
			float x_move = Input.GetAxis("Horizontal") * m_speed;
			float y_move = Input.GetAxis("Vertical") * m_speed;
#endif
			m_rb.velocity = new Vector3(x_move, m_rb.velocity.y, y_move);
		}

		private void OnDestroy() { m_playerEvent.Invoke(null); }

		private void OnDamage(float dammage) {
			m_life -= dammage;
			if (m_ui) {
				m_ui.text = m_life.ToString();
			}
		}
	}
}