namespace m21cerutti.ClapClapEvent.Samples {
	using System.IO;

	using BUS.Runtime;

	using UnityEngine;
	using UnityEngine.SceneManagement;
	using UnityEngine.UI;

#if USE_NEWINPUT
	using UnityEngine.InputSystem;
#endif

	public class ClapClapSamplesMenu : MonoBehaviour {

		[SerializeField] private GameObject m_buttonPrefab;
		[SerializeField] private GameObject m_buttonHolder;
		[SerializeField] private SceneReference[] m_scenesSamples;

		private void Start() {
			foreach (SceneReference refScene in m_scenesSamples) {
				string name_scene = Path.GetFileNameWithoutExtension(refScene.scenePath);
				Button button = Instantiate(m_buttonPrefab, m_buttonHolder.transform).GetComponent<Button>();
				button.name = name_scene + " (unload)";
				Text t = button.GetComponentInChildren<Text>();
				t.text = name_scene + " (unload)";
				t.color = Color.red;
				button.onClick.AddListener(() => {
					HandleScene(refScene, t);
				});
			}
		}

		private void Update() {
#if USE_NEWINPUT
			if (Keyboard.current.escapeKey.wasPressedThisFrame) {
#else
			if (Input.GetButtonDown("Cancel")) {
#endif
				OnQuitGame();
			}
		}

		public void OnQuitGame() { Application.Quit(); }

		private static void HandleScene(SceneReference refScene, Text t) {
			Scene scene = SceneManager.GetSceneByPath(refScene.scenePath);
			string nameScene = Path.GetFileNameWithoutExtension(refScene.scenePath);
			if (scene.isLoaded) {
				SceneManager.UnloadSceneAsync(refScene);
				t.text = nameScene + " (unload)";
				t.color = Color.red;
			}
			else {
				SceneManager.LoadScene(refScene, LoadSceneMode.Additive);
				t.text = nameScene;
				t.color = Color.white;
			}
		}
	}
}