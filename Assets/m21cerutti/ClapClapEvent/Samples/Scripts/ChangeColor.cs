namespace m21cerutti.ClapClapEvent.Samples {
	using UnityEngine;

	public class ChangeColor : MonoBehaviour {
		[SerializeField] private Color[] m_colors;
		private Renderer m_renderer;

		private void Start() { m_renderer = GetComponent<Renderer>(); }

		public void OnChangeColor(int index) { m_renderer.material.color = m_colors[index]; }

		public void OnDeactivate() { gameObject.SetActive(false); }
	}
}