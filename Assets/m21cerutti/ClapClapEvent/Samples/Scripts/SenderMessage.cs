namespace m21cerutti.ClapClapEvent.Samples {
	using System.Collections;

	using CustomEvents;

	using UnityEngine;

	public class SenderMessage : MonoBehaviour {
		[SerializeField] private StringEvent m_ev;
		[SerializeField] private string m_message = "Otaria !!";

		[SerializeField] private float m_timer = 2.0f;
		private bool m_isSending;

		private void Update() {
			if (!m_isSending && m_message.Length > 0) {
				StartCoroutine(_SendMessage());
			}
		}

		private IEnumerator _SendMessage() {
			m_isSending = true;

			yield return new WaitForSecondsRealtime(m_timer);
			m_message = m_message.Remove(m_message.Length - 1);
			m_ev.Invoke(m_message);
			m_isSending = false;
		}
	}
}