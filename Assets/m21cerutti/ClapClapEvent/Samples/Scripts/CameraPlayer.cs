namespace m21cerutti.ClapClapEvent.Samples {
	using UnityEngine;
	using UnityEngine.Animations;

	public class CameraPlayer : MonoBehaviour {
		private LookAtConstraint m_constraint;

		private void Start() { m_constraint = GetComponent<LookAtConstraint>(); }

		public void OnPlayer(GameObject player) {
			if (player == null) {
				m_constraint.RemoveSource(0);
			}
			else {
				m_constraint.AddSource(new ConstraintSource {
					sourceTransform = player.transform,
					weight = 1.0f
				});
			}
		}
	}
}